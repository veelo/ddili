Ddoc

<div class="on_ust">
$(IMG_D)

<div class="d_harfi_yan">

---

import std.stdio;

void main()
{
    writeln("Merhaba dünya!");
}

---

</div>
</div>

<div class="main_page_content">

$(H5 D Programlama Dili'nin pdf hali)

$(P
$(LINK2 /ders/d/, D Programlama Dili) kitabını pdf olarak indirmek için $(LINK2 /ders/d/pdf_indir.html, buraya tıklayın. $(IMG pdficon_small.gif))
)

$(H5 $(LINK2 /tanitim/, Tanıtım))

$(P
D, Walter Bright tarafından C++'dan türetilmiş üst düzey bir sistem programlama dilidir. Büyük C++ ustalarından Andrei Alexandrescu'nun $(LINK2 /makale/neden_d.html, Neden D) isimli makalesi tanıtıcı bilgiler verir.
)

$(H5 $(LINK2 /ders/, D.ershane (Kitaplar)))
$(P
Temelden başlayarak adım adım D programcılığı öğrenebileceğiniz bir bölüm
)

$(H5 $(LINK2 /forum/, Forum))

$(P
D sohbetlerine katılabileceğiniz canlı bir ortam
)

$(H5 $(LINK2 /wiki/index.php?title=Ddili_Projeleri, Projeler))

$(P
Katkıda bulunurken D deneyiminizi de arttıracağınız projeler
)

$(H5 $(LINK2 /sunum/, Sunumlar))

$(P
Her yıl yapılması planlanan, D dilini tanıtmayı amaçlayan, ve D programcılarına yüz yüze tanışma olanağı sunan seminer dizisi
)

$(H5 $(LINK2 /wiki/, Wiki))

$(P
Türkçe belgeler ve program örnekleri bulabileceğiniz, ve katkılarınızla geliştireceğiniz bir paylaşım alanı
)

</div>

Macros:
        SUBTITLE=Ana Sayfa

        DESCRIPTION=Türkçe D programlama dili kitabı, forum, dil ile ilgili belgeler, makaleler, kod örnekleri, forum, haberler, ve başka çeşitli bilgiler

        KEYWORDS=d programlama dili kitap ders forum

       BREADCRUMBS=$(BREADCRUMBS_INDEX)
