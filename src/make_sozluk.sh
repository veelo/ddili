#!/bin/bash

echo > sozluk.ddoc
echo -n >> sozluk.ddoc
sed 's/[ ]*\(.*\)\b[ ]*|[ ]*\(.*\)\b[ ]*|[ ]*\(.*\)\b[ ]*|[ ]*\(.*\)/\1 = <div class=\"mini_sozluk_sozcuk\">$(B \2:) [\3], \4<\/div>/' < sozluk.txt >> sozluk.ddoc

echo > sozluk_body.ddoc
echo -n "SOZLUK_BODY=" >> sozluk_body.ddoc
sed 's/[ ]*\(.*\)\b[ ]*|[ ]*\(.*\)\b[ ]*|[ ]*\(.*\)\b[ ]*|[ ]*\(.*\)/<div class=\"sozcuk\">$(B \2:) [\3], \4<\/div>/' < sozluk.txt | sed '/^$/d' >> sozluk_body.ddoc

cp sozluk.d ders/d/sozluk.d
