Ddoc

<div class="imprint">

$(P
<span class="copyright_title">D Programlama Dili, Birinci Basım</span>
)

$(BR)

$(P
Sürüm: <xi:include href="pdf_surum.txt" parse="text"/>
)

$(BR)

$(P
Bu kitabın son sürümünü $(LINK2 http://ddili.org/ders/d, sitesinden) edinebilirsiniz.
)

$(BR)
$(BR)
$(BR)

$(P
Copyleft (ɔ) 2009-2015 Ali Çehreli
)

$(BR)

$(P
<img alt="Creative Commons License" style="border-width:0" src="$(ROOT_DIR)/image/cc_88x31.png" />
)

$(BR)
<p style="margin-left:1em; text-indent:0em">
Bu eser, Creative Commons'ın $(I Atıf-Gayriticari-LisansDevam 4.0 Uluslararası lisansı) ile lisanslanmıştır. Bu lisansın tam metni için http://creativecommons.org/licenses/by-nc-sa/4.0/deed.tr/ adresine gidiniz.
</p>

$(BR)
$(BR)
$(BR)

$(P
İngilizce editör: $(LINK2 http://www.luismarques.eu, Luís Marques)
)
$(BR)
$(P
Kapak resmi: $(LINK2 mailto:sarah@reeceweb.com, Sarah Reece)
)
$(BR)
$(P
Kapak tasarımı: $(LINK2 http://izgiyapici.com/, İzgi Yapıcı)
)
$(BR)
$(P
Basımcı: Ali Çehreli
)
</div>

Macros:
        SUBTITLE=Copyleft

        DESCRIPTION=D Programlama Dili'nin copyleft sayfası

        KEYWORDS=copyright
