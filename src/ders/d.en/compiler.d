Ddoc

$(DERS_BOLUMU $(IX compilation) Compilation)

$(P
We have seen that the two tools that are used most in D programming are $(I the text editor) and $(I the compiler). D programs are written in text editors.
)

$(P
The concept of compilation and the function of the compiler must also be understood when using $(I compiled) languages like D.
)

$(H5 $(IX machine code) Machine code)

$(P
$(IX CPU) $(IX microprocessor) The brain of the computer is the microprocessor (or the CPU, short for $(I central processing unit)). Telling the CPU what to do is called $(I coding), and the instructions that are used when doing so are called $(I machine code).
)

$(P
Most CPU architectures use machine code specific to that particular architecture. These machine code instructions are determined under hardware constraints during the design stage of the architecture. At the lowest level these machine code instructions are implemented as electrical signals. Because the ease of coding is not a primary consideration at this level, writing programs directly in the form of the machine code of the CPU is a very difficult task.
)

$(P
These machine code instructions are special numbers, which represent various operations supported by the CPU. For example, for an imaginary 8-bit CPU, the number 4 might represent the operation of loading, the number 5 might represent the operation of storing, and the number 6 might represent the operation of incrementing. Assuming that the leftmost 3 bits are the operation number and the rightmost 5 bits are the value that is used in that operation, a sample program in machine code for this CPU might look like the following:
)

$(MONO
$(B
Operation   Value            Meaning)
   100      11110        LOAD      11110
   101      10100        STORE     10100
   110      10100        INCREMENT 10100
   000      00000        PAUSE
)

$(P
Being so close to hardware, machine code is not suitable for representing higher level concepts like $(I a playing card) or $(I a student record).
)

$(H5 $(IX programming language) Programming language)

$(P
Programming languages are designed as efficient ways of programming a CPU, capable of representing higher-level concepts. Programming languages do not have to deal with hardware constraints; their main purposes are ease of use and expressiveness. Programming languages are easier for humans to understand, closer to natural languages:
)

$(MONO
if (a_card_has_been_played()) {
   display_the_card();
}
)

$(P
However, programming languages adhere to much more strict and formal rules than any spoken language.
)

$(H5 $(IX compiled language) Compiled languages)

$(P
In some programming languages, the program instructions must be compiled before becoming a program to be executed. Such languages produce fast executing programs but the development process involves two main steps: writing the program and compiling it.
)

$(P
In general, compiled languages help with catching programming errors before the program even starts running.
)

$(P
D is a compiled language.
)

$(H5 $(IX interpreted language) Interpreted languages)

$(P
Some programming languages don't require compilation. Such languages are called $(I interpreted languages). Programs can be executed directly from the hand-written program. Some examples of interpreted languages are Python, Ruby, and Perl. Because there is no compilation step, program development can be easier for these languages. On the other hand, as the instructions of the program must be parsed to be interpreted every time the program is executed, programs that are written in these languages are slower than their equivalents written in compiled languages.
)

$(P
In general, for an interpreted language, many types of errors in the program cannot be discovered until the program starts running.
)

$(H5 $(IX compiler) Compiler)

$(P
The purpose of a compiler is translation: It translates programs written in a programming language into machine code. This translation is called $(I compilation). Every compiler understands a particular programming language and is described as a compiler of that language, as in "a D compiler".
)

$(H5 $(IX error, compilation) Compilation error)

$(P
As the compiler compiles a program according to the rules of the language, it stops the compilation as soon as it comes across $(I illegal) instructions. Illegal instructions are the ones that are outside the specifications of the language. Problems like a mismatched parenthesis, a missing semicolon, a misspelled keyword, etc. all cause compilation errors.
)

$(P
The compiler may also emit a $(I compilation warning) when it sees a suspicious piece of code that may cause concern but not necessarily an error. However, warnings almost always indicate an actual error or bad style, so it is a common practice to consider most or all warnings as errors. The $(C dmd) compiler switch to enable warnings as errors is $(C -w).
)

$(Ergin)

Macros:
        SUBTITLE=Compiler

        DESCRIPTION=The introduction of the compiler and compiled languages

        KEYWORDS=d programming language tutorial book
