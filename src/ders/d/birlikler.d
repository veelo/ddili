Ddoc

$(DERS_BOLUMU Birlikler)

$(P
Birlikler, birden fazla üyenin aynı bellek alanını paylaşmalarını sağlarlar. D'ye C dilinden geçmiş olan alt düzey bir olanaktır.
)

$(P
İki fark dışında yapılarla aynı şekilde kullanılır:
)

$(UL

$(LI $(C struct) yerine $(C union) anahtar sözcüğü ile tanımlanır)

$(LI üyeleri aynı bellek alanını paylaşırlar; birbirlerinden bağımsız değillerdir)

)

$(P
Yapılar gibi, birliklerin de üye işlevleri bulunabilir.
)

$(P
Aşağıdaki örnek programlar derlendikleri ortamın 32 bit veya 64 bit olmasına bağlı olarak farklı sonuçlar üreteceklerdir. Bu yüzden, bu bölümdeki programları derlerken $(C -m32) derleyici seçeneğini kullanmanızı öneririm. Aksi taktirde sizin sonuçlarınız aşağıda gösterilenlerden farklı olabilir.
)

$(P
Şimdiye kadar çok karşılaştığımız yapı türlerinin kullandıkları bellek alanı bütün üyelerini barındıracak kadar büyüktü:
)

---
struct Yapı
{
    int i;
    double d;
}

// ...

    writeln(Yapı.sizeof);
---

$(P
Dört baytlık $(C int)'ten ve sekiz baytlık $(C double)'dan oluşan o yapının büyüklüğü 12'dir:
)

$(SHELL_SMALL
12
)

$(P
Aynı şekilde tanımlanan bir birliğin büyüklüğü ise, üyeleri aynı bellek bölgesini paylaştıkları için, üyelerden en büyüğü için gereken yer kadardır:
)

---
$(HILITE union) Birlik
{
    int i;
    double d;
}

// ...

    writeln(Birlik.sizeof);
---

$(P
Dört baytlık $(C int) ve sekiz baytlık $(C double) aynı alanı paylaştıkları için bu birliğin büyüklüğü en büyük üye için gereken yer kadardır:
)

$(SHELL_SMALL
8
)

$(P
Bunun bellek kazancı sağlayan bir olanak olduğunu düşünmeyin. Aynı bellek alanına birden fazla bilgi sığdırmak olanaksızdır. Birliklerin yararı, aynı bölgenin farklı zamanlarda farklı türden bilgiler için kullanılabilmesidir. Belirli bir anda tek bir üye saklanabilir. Birliklerin yararlarından birisi, her ortamda aynı şekilde çalışmasa da, bilginin parçalarına diğer üyeler yoluyla erişilebilmesidir.
)

$(P
Yukarıdaki birliği oluşturan sekiz baytın bellekte nasıl durduklarını, ve üyeler için nasıl kullanıldıklarını şöyle gösterebiliriz:
)

$(MONO

       0      1      2      3      4      5      6      7
───┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬──────┬───
   │$(HILITE <───  int için 4 bayt  ───>)                            │
   │$(HILITE <───────────────  double için 8 bayt  ────────────────>)│
───┴──────┴──────┴──────┴──────┴──────┴──────┴──────┴──────┴───
)

$(P
Ya sekiz baytın hepsi birden $(C double) üye için kullanılır, ya da ilk dört bayt $(C int) üye için kullanılır ve gerisine dokunulmaz.
)

$(P
Ben örnek olarak iki üye kullandım; birlikleri istediğiniz kadar üye ile tanımlayabilirsiniz. Üyelerin hepsi aynı alanı paylaşırlar.
)

$(P
Aynı bellek bölgesinin kullanılıyor olması ilginç sonuçlar doğurabilir. Örneğin, birliğin bir $(C int) ile ilklenmesi ama bir $(C double) olarak kullanılması, baştan kestirilemeyecek $(C double) değerleri verebilir:
)

---
    auto birlik = Birlik(42);    // int üyenin ilklenmesi
    writeln(birlik.d);           // double üyenin kullanılması
---

$(P
$(C int) üyeyi oluşturan dört baytın 42 değerini taşıyacak şekilde kurulmaları, $(C double) üyenin değerini de etkiler:
)

$(SHELL_SMALL
4.9547e-270
)

$(P
Mikro işlemcinin bayt sıralarına bağlı olarak $(C int) üyeyi oluşturan dört bayt bellekte 0|0|0|42, 42|0|0|0, veya daha başka bir düzende bulunabilir. Bu yüzden yukarıdaki $(C double) üyenin değeri başka ortamlarda daha farklı da olabilir.
)

$(H5 İsimsiz birlikler)

$(P
İsimsiz birlikler, içinde bulundukları bir yapının hangi üyelerinin paylaşımlı olarak kullanıldıklarını belirlerler:
)

---
struct BirYapı
{
    int birinci;

    union
    {
        int ikinci;
        int üçüncü;
    }
}

// ...

    writeln(BirYapı.sizeof);
---

$(P
Yukarıdaki yapının son iki üyesi aynı alanı paylaşırlar ve bu yüzden yapı, toplam iki $(C int)'in büyüklüğü kadar yer tutar. Birlik üyesi olmayan $(C birinci) için gereken 4 bayt, ve $(C ikinci) ile $(C üçüncü)'nün paylaştıkları 4 bayt:
)

$(SHELL_SMALL
8
)

$(H5 Başka bir türün baytlarını ayrıştırmak)

$(P
Birlikler, türleri oluşturan baytlara teker teker erişmek için kullanılabilirler. Örneğin aslında 32 bitten oluşan IPv4 adreslerinin 4 bölümünü elde etmek için bu 32 biti paylaşan 4 baytlık bir dizi kullanılabilir. Adres değerini oluşturan üye ve dört bayt bir birlik olarak şöyle bir araya getirilebilir:
)

---
$(CODE_NAME IpAdresi)
union IpAdresi
{
    uint değer;
    ubyte[4] baytlar;
}
---

$(P
O birliği oluşturan iki üye, aynı belleği şu şekilde paylaşırlar:
)

$(MONO

          0            1            2            3
───┬────────────┬────────────┬────────────┬────────────┬───
   │$(HILITE <───────  IPv4 adresini oluşturan 32 bit  ────────>)│
   │ baytlar[0] │ baytlar[1] │ baytlar[2] │ baytlar[3] │
───┴────────────┴────────────┴────────────┴────────────┴───
)

$(P
Bu birlik, daha önceki bölümlerde 192.168.1.2 adresinin değeri olarak karşılaştığımız 0xc0a80102 ile ilklendiğinde, $(C baytlar) dizisinin elemanları teker teker adresin dört bölümüne karşılık gelirler:
)

---
$(CODE_XREF IpAdresi)void main()
{
    auto adres = IpAdresi(0xc0a80102);
    writeln(adres$(HILITE .baytlar));
}
---

$(P
Adresin bölümleri, bu programı denediğim ortamda alışık olunduğundan ters sırada çıkmaktadır:
)

$(SHELL_SMALL
[2, 1, 168, 192]
)

$(P
Bu, programı çalıştıran mikro işlemcinin küçük soncul olduğunu gösterir. Başka ortamlarda başka sırada da çıkabilir.
)

$(P
Bu örnekte özellikle belirtmek istediğim, birlik üyelerinin değerlerinin belirsiz olabilecekleridir. Birlikler, ancak ve ancak tek bir üyeleri ile kullanıldıklarında beklendiği gibi çalışırlar. Hangi üyesi ile kurulmuşsa, birlik nesnesinin yaşamı boyunca o üyesi ile kullanılması gerekir. O üye dışındaki üyelere erişildiğinde ne tür değerlerle karşılaşılacağı ortamdan ortama farklılık gösterebilir.
)

$(P
Bu bölümle ilgisi olmasa da, $(C core.bitop) modülünün $(C bswap) işlevinin bu konuda yararlı olabileceğini belirtmek istiyorum. $(C bswap), kendisine verilen $(C uint)'in baytları ters sırada olanını döndürür. $(C std.system) modülündeki $(C endian) değerinden de yararlanırsak, küçük soncul bir ortamda olduğumuzu şöyle belirleyebilir ve yukarıdaki IPv4 adresini oluşturan baytları tersine çevirebiliriz:
)

---
import std.system;
import core.bitop;

// ...

    if (endian == Endian.littleEndian) {
        adres.değer = bswap(adres.değer);
    }
---

$(P
$(C Endian.littleEndian) değeri sistemin küçük soncul olduğunu, $(C Endian.BigEndian) değeri de büyük soncul olduğunu belirtir. Yukarıdaki dönüşüm sonucunda IPv4 adresinin bölümleri alışık olunan sırada çıkacaktır:
)

$(SHELL_SMALL
[192, 168, 1, 2]
)

$(P
Bunu yalnızca birliklerle ilgili bir kullanım örneği olarak gösterdim. Normalde IPv4 adresleriyle böyle doğrudan ilgilenmek yerine, o iş için kullanılan bir kütüphanenin olanaklarından yararlanmak daha doğru olur.
)

$(H5 Protokol örneği)

$(P
Bazı protokollerde, örneğin ağ protokollerinde, bazı baytların anlamı başka bir üye tarafından belirleniyor olabilir. Ağ pakedinin daha sonraki bir bölümü, o üyenin değerine göre farklı bir şekilde kullanılıyor olabilir:
)

---
struct Adres
{
    // ...
}

struct BirProtokol
{
    // ...
}

struct BaşkaProtokol
{
    // ...
}

enum ProtokolTürü { birTür, başkaTür }

struct AğPakedi
{
    Adres hedef;
    Adres kaynak;
    ProtokolTürü $(HILITE tür);

    $(HILITE union)
    {
        BirProtokol birProtokol;
        BaşkaProtokol başkaProtokol;
    }

    ubyte[] geriKalanı;
}
---

$(P
Yukarıdaki $(C AğPakedi) yapısında hangi protokol üyesinin geçerli olduğu $(C tür)'ün değerinden anlaşılabilir, programın geri kalanı da yapıyı o değere göre kullanır.
)

$(H5 Ne zaman kullanmalı)

$(P
Bayt paylaşmak gibi kavramlar oldukça alt düzey kabul edilecekleri için, birlikler günlük tasarımlarımızda hemen hemen hiç kullanılmazlar.
)

$(P
Birliklerle C kütüphanelerinde de karşılaşılabilir.
)

Macros:
        SUBTITLE=Birlikler

        DESCRIPTION=D'nin farklı türden değişkenleri aynı bellek bölgesinde depolamaya olanak veren birlik (union) olanağı

        KEYWORDS=d programlama dili ders bölümler öğrenmek tutorial birlik union

SOZLER=
$(alt_duzey)
$(bayt_sirasi)
$(birlik)
$(buyuk_soncul)
$(kucuk_soncul)
$(yapi)
