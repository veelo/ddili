Ddoc

$(DERS_BOLUMU Derleme)

$(P
D programcılığında en çok kullanılan iki aracın $(I metin düzenleyici) ve $(I derleyici) olduklarını gördük. Programlar metin düzenleyicilerde yazılırlar.
)

$(P
D gibi dillerde derleme kavramı ve derleyicinin işlevi de hiç olmazsa kaba hatlarıyla mutlaka bilinmelidir.
)

$(H5 Makine kodu)

$(P
Bilgisayarın beyni CPU denen mikro işlemcidir. Mikro işlemciye ne işler yapacağını bildirmeye $(I kodlama), bu bildirimlere de $(I kod) denir.
)

$(P
Her mikro işlemci modelinin kendisine has kodları vardır. Her mikro işlemcinin nasıl kodlanacağına mimari tasarımı aşamasında ve donanım kısıtlamaları gözetilerek karar verilir. Bu kodlar çok alt düzeyde elektronik sinyaller olarak gerçekleştirilirler. Bu tasarımda kodlama kolaylığı geri planda kaldığı için, doğrudan mikro işlemciyi kodlayarak program yazmak çok güç bir iştir.
)

$(P
Mikro işlemcinin adının parçası olan $(I işlem) kavramı, özel sayılar olarak belirlenmiştir. Örneğin kodları 8 bit olan hayalî bir işlemcide 4 sayısı yükleme işlemini, 5 sayısı depolama işlemini, 6 sayısı da arttırma işlemini gösteriyor olabilir. Bu hayalî işlemcide soldaki 3 bitin işlem sayısı ve sağdaki 5 bitin de o işlemde kullanılacak değer olduğunu düşünürsek, bu mikro işlemcinin makine kodu şöyle olabilir:
)

$(MONO
$(B
İşlem   Değer           Anlamı)
 100    11110        YÜKLE  11110
 101    10100        DEPOLA 10100
 110    10100        ARTTIR 10100
 000    00000        BEKLE
)

$(P
Makine kodunun donanıma bu kadar yakın olması, $(I oyun kağıdı) veya $(I öğrenci kayıtları) gibi üst düzey kavramların bilgisayarda temsil edilmelerini son derece güç hale getirir.
)

$(H5 Programlama dili)

$(P
Mikro işlemcileri kullanmanın daha etkin yolları aranmış, ve çözüm olarak üst düzey kavramları ifade etmeye elverişli programlama dilleri geliştirilmiştir. Bu dillerin donanım kaygıları olmadığı için, özellikle kullanım kolaylığı ve ifade gücü gözetilerek tasarlanmışlardır. Programlama dilleri insanlara uygun dillerdir ve çok kabaca konuşma dillerine benzerler:
)

$(MONO
if (ortaya_kağıt_atılmış_mı()) {
   oyun_kağıdını_göster();
}
)

$(P
Buna rağmen, programlama dilleri çok daha sıkı kurallara sahiptirler.
)

$(P
Programlama dillerinin bir sorunu, anahtar sözcüklerinin geleneksel olarak İngilizce olmasıdır. Neyse ki bunlar kolayca öğrenebilecek kadar az sayıdadır. Örneğin $(C if)'in "eğer" anlamına geldiğini bir kere öğrenmek yeter.
)

$(H5 Derlemeli dil)

$(P
Bu gibi dillerde yazılan programın çalıştırılır hale gelmeden önce derlenmesi gerekir. Bu yöntem çok hızlı çalışan programlar üretir; ama programı yazmanın yanında bir de derlemek gerektiği için, program geliştirme aşaması daha külfetlidir.
)

$(P
Aynı nedenden dolayı ve genel olarak, derlemeli dil programlarındaki hatalar daha program çalışmaya başlamadan yakalanabilirler.
)

$(P
D, derlemeli bir dildir.
)

$(H5 Yorumlamalı dil)

$(P
Bazı programlama dilleri derleyici gerektirmezler. Bu gibi dillere $(I yorumlamalı dil) denir. Yorumlamalı dillerde yazılan programlar derlenmeleri gerekmeden hemen çalıştırılabilirler. Bu dillere örnek olarak Python, Ruby, ve Perl'ü gösterebiliriz. Derleme aşaması olmadığı için bu diller program geliştirmeyi çabuklaştırırlar. Bir sakıncaları, her çalıştırıldıklarında program metninin baştan taranmasının ve makine kodu karşılıklarının çalışma zamanında bulunmasının gerekmesidir. Bu yüzden, yorumlamalı dillerde yazılan programlar derlemeli dillerde yazılan eşdeğerlerinden genel olarak daha yavaş çalışırlar.
)

$(P
Genel olarak, yorumlamalı bir dilde yazılmış olan bir programdaki çok çeşit hata ancak program çalışmaya başladıktan sonra yakalanabilir.
)

$(H5 Derleyici)

$(P
Derleyicinin görevi aracılıktır: insanların anladığı programlama dilini mikro işlemcinin anladığı kodlara çevirir. Bu işleme $(I derleme) denir. Her derleyici belirli bir programlama dilini bilir ve o dilin derleyicisi olarak anılır: "D derleyicisi" gibi.
)

$(H5 Derleme hatası)

$(P
Derleyiciler programı dilin kurallarına uygun olarak derledikleri için, kural dışı kodlar gördüklerinde bir hata mesajı vererek sonlanırlar. Örneğin kapanmamış bir parantez, unutulmuş bir noktalı virgül, yanlış yazılmış bir anahtar sözcük, vs. derleme hatasına yol açar.
)

$(P
Derleyici bazen de kod açıkça kural dışı olmadığı halde, programcının yanlış yapmasından şüphelenebilir ve bu konuda uyarı mesajı verebilir. Program derlenmiş bile olsa, her zaman için uyarıları da hata gibi kabul edip, uyarıya neden olan kodu değiştirmek iyi olur.
)

Macros:
        SUBTITLE=Derleyici

        DESCRIPTION=Derleyici kavramının tanıtılması

        KEYWORDS=d programlama dili dersleri öğrenmek tutorial derleyici makina

SOZLER= 
$(anahtar_sozcuk)
$(bit)
$(derleyici)
$(makine_kodu)
$(metin_duzenleyici)
$(mikro_islemci)
$(program)
