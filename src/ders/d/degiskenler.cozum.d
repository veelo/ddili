Ddoc

$(COZUM_BOLUMU Değişkenler)

$(OL

$(LI &nbsp;

---
import std.stdio;

void main()
{
    double kur = 2.11;
    int adet = 20;

    writeln(kur, " kurundan ", adet, " avro bozdurdum.");
}
---
)

)

Macros:
        SUBTITLE=Değişkenler Problem Çözümü

        DESCRIPTION=İlk D programlama dili dersi çözümleri: Değişkenler

        KEYWORDS=d programlama dili dersleri öğrenmek tutorial değişkenler problem çözüm
