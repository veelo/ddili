Ddoc

<div class="imprint">

$(P
<span class="copyright_title">Programming in D, First Edition</span>
)

$(BR)

$(P
Revision: <xi:include href="pdf_surum.txt" parse="text"/>
)

$(BR)

$(P
The most recent edition of this book is available $(LINK2 http://ddili.org/ders/d.en, online).
)

$(BR)
$(BR)
$(BR)

$(P
Copyleft (ɔ) 2009-2015 Ali Çehreli
)

$(BR)

$(P
<img alt="Creative Commons License" style="border-width:0" src="$(ROOT_DIR)/image/cc_88x31.png" />
)
$(BR)
<p style="margin-left:1em; text-indent:0em">
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/.
</p>

$(BR)
$(BR)
$(BR)

$(P
Edited by $(LINK2 http://www.luismarques.eu, Luís Marques)
)

$(BR)

$(P
Cover illustration by $(LINK2 mailto:sarah@reeceweb.com, Sarah Reece)
)

$(BR)

$(P
Cover design by $(LINK2 http://izgiyapici.com/, İzgi Yapıcı)
)

$(BR)

$(P
Published by Ali Çehreli
)

$(BR)

</div>

Macros:
        SUBTITLE=Copyleft

        DESCRIPTION=The copyleft page of Programming in D

        KEYWORDS=copyright
