Ddoc

$(DERS_BOLUMU Modüller ve Kütüphaneler)

$(P
D programlarını ve kütüphanelerini oluşturan en alt yapısal birimler modüllerdir.
)

$(P
D'nin modül kavramı çok basit bir temel üzerine kuruludur: Her kaynak dosya bir modüldür. Bu tanıma göre, şimdiye kadar deneme programlarımızı yazdığımız tek kaynak dosya bile bir modüldür.
)

$(P
Her modülün ismi, dosya isminin $(C .d) uzantısından önceki bölümü ile aynıdır ve kaynak dosyanın en başına yazılan $(C module) anahtar sözcüğü ile belirtilir. Örneğin, "kedi.d" isimli bir kaynak dosyanın modül ismi aşağıdaki gibi belirtilir:
)

---
module kedi;

class Kedi
{
    // ...
}
---

$(P
Eğer modül bir pakedin parçası değilse $(C module) satırı isteğe bağlıdır. (Paketleri biraz aşağıda göreceğiz.) Yazılmadığı zaman otomatik olarak dosyanın isminin $(C .d)'den önceki bölümü kullanılır.
)

$(H5 $(C static this()) ve $(C static ~this()))

$(P
Modül düzeyinde tanımlanan $(C static this()) ve $(C static ~this()), yapı ve sınıflardaki eşdeğerleri ile aynı anlamdadır:
)

---
module kedi;

static this()
{
    // ... modülün ilk işlemleri ...
}

static ~this()
{
    // ... modülün son işlemleri ...
}
---

$(P
Bu işlevler her iş parçacığında ayrı ayrı işletilir. (Çoğu program yalnızca $(C main())'in işlediği tek iş parçacığından oluşur.) İş parçacıklarının sayısından bağımsız olarak bütün programda tek kere işletilmesi gereken kodlar ise (örneğin, $(C immutable) değişkenlerin ilklenmeleri) $(C shared static this()) ve $(C shared static ~this()) işlevlerinde tanımlanırlar. Bunları daha sonraki $(LINK2 /ders/d/es_zamanli_shared.html, Veri Paylaşarak Eş Zamanlı Programlama bölümünde) göreceğiz.
)

$(H5 Dosya ve modül isimleri)

$(P
D programlarını Unicode olarak oluşturma konusunda şanslıyız; bu, hangi ortamda olursa olsun geçerlidir. Ancak, dosya sistemleri konusunda aynı serbesti bulunmaz. Örneğin Windows işletim sistemlerinin standart dosya sistemleri dosya isimlerinde büyük/küçük harf ayrımı gözetmezken, Linux sistemlerinde büyük/küçük harfler farklıdır. Ayrıca çoğu dosya sistemi, dosya isimlerinde kullanılabilecek karakterler konusunda kısıtlamalar getirir.
)

$(P
O yüzden, programlarınızın taşınabilir olmaları için dosya isimlerinde yalnızca ASCII küçük harfler kullanmanızı öneririm. Örneğin yukarıdaki $(C Kedi) sınıfı ile birlikte kullanılacak olan bir $(C Köpek) sınıfının modülünün dosya ismini "kopek.d" olarak seçebiliriz.
)

$(P
Bu yüzden modülün ismi de ASCII harflerden oluşur:
)

---
module kopek;    // ASCII harflerden oluşan modül ismi

class Köpek      // Unicode harflerden oluşan program kodu
{
    // ...
}
---

$(H5 Paketler)

$(P
Modüllerin bir araya gelerek oluşturdukları yapıya $(I paket) denir. D'nin paket kavramı da çok basittir: Dosya sisteminde aynı klasörde bulunan bütün modüller aynı pakedin parçası olarak kabul edilirler. Pakedi içeren klasörün ismi de modül isimlerinin baş tarafını oluşturur.
)

$(P
Örneğin yukarıdaki "kedi.d" ve "kopek.d" dosyalarının "hayvan" isminde bir klasörde bulunduklarını düşünürsek, modül isimlerinin başına klasör ismini yazmak, onları aynı pakedin modülleri yapmaya yeter:
)

---
module $(HILITE hayvan.)kedi;

class Kedi
{
    // ...
}
---

$(P
Aynı şekilde $(C kopek) modülü için de:
)

---
module $(HILITE hayvan.)kopek;

class Köpek
{
    // ...
}
---

$(P
$(C module) satırı bir pakedin parçası olan modüllerde zorunludur.
)

$(P
Paket isimleri dosya sistemi klasörlerine karşılık geldiği için, iç içe klasörlerde bulunan modüllerin paket isimleri de o klasör yapısının eşdeğeridir. Örneğin "hayvan" klasörünün altında bir de "omurgalilar" klasörü olsa, oradaki bir modülün paket ismi bu klasörü de içerir:
)

---
module hayvan.omurgalilar.kedi;
---

$(P
Kaynak dosyaların ne derece dallanacağı programın büyüklüğüne ve tasarımına bağlıdır. Küçük bir programın bütün dosyalarının tek bir klasörde bulunmasında bir sakınca yoktur. Öte yandan, dosyaları belirli bir düzen altına almak için klasörleri gerektiği kadar dallandırmak da mümkündür.
)

$(H5 Modüllerin programda kullanılmaları)

$(P
Şimdiye kadar çok kullandığımız $(C import) anahtar sözcüğü, bir modülün başka bir modüle tanıtılmasını ve o modül içinde kullanılabilmesini sağlar:
)

---
import std.stdio;
---

$(P
$(C import)'tan sonra yazılan modül ismi, eğer varsa paket bilgisini de içerir. Yukarıdaki koddaki $(C std.), standart kütüphaneyi oluşturan modüllerin $(C std) isimli pakette bulunduklarını gösterir.
)

$(P
Benzer şekilde, $(C hayvan.kedi) ve $(C hayvan.kopek) modülleri bir "deneme.d" dosyasında şu şekilde bildirilir:
)

---
module deneme;          // bu modülün ismi

import hayvan.kedi;     // kullandığı bir modül
import hayvan.kopek;    // kullandığı başka bir modül

void main()
{
    auto kedi = new Kedi();
    auto köpek = new Köpek();
}
---

$(P $(I Not: Aşağıda anlatıldığı gibi, yukarıdaki programın derlenip oluşturulabilmesi için o modül dosyalarının da bağlayıcıya derleme satırında bildirilmeleri gerekir.)
)

$(P
Birden fazla modül aynı anda eklenebilir:
)

---
import hayvan.kedi, hayvan.kopek;
---

$(H6 Seçerek eklemek)

$(P
Bir modüldeki bütün isimlerin hepsini birden eklemek yerine içindeki isimler tek tek seçilerek eklenebilir:
)

---
import std.stdio $(HILITE : writeln;)

// ...

    write$(HILITE f)ln("Merhaba %s.", isim);    $(DERLEME_HATASI)
---

$(P
Yukarıdaki kod derlenemez çünkü $(C stdio) modülünden yalnızca $(C writeln) eklenmiştir; $(C writefln) eklenmemiştir.
)

$(P
İsimleri seçerek eklemek hepsini birden eklemekten daha iyidir çünkü $(I isim çakışmalarının) olasılığı daha azdır. Biraz aşağıda bir örneğini göreceğimiz gibi, isim çakışması iki farklı modüldeki aynı ismin eklenmesiyle oluşur.
)

$(P
Ek olarak, yalnızca belirtilen isimler derleneceğinden derleme sürelerinin kısalacağı da beklenebilir. Öte yandan, her kullanılan ismin ayrıca belirtilmesini gerektirdiğinden seçerek eklemek daha fazla emek gerektirir.
)

$(P
Kod örneklerini kısa tutmak amacıyla bu kitapta seçerek ekleme olanağından yararlanılmamaktadır.
)

$(H6 Yerel $(C import) satırları)

$(P
Bu kitaptaki bütün $(C import) satırlarını hep programların en başlarına yazdık:
)

---
import std.stdio;     $(CODE_NOTE en başta)
import std.string;    $(CODE_NOTE en başta)

// ... modülün geri kalanı ...
---

$(P
Aslında modüller herhangi başka bir satırda da eklenebilirler. Örneğin, aşağıdaki programdaki iki işlev ihtiyaç duydukları farklı modülleri kendi yerel kapsamlarında eklemekteler:
)

---
string mesajOluştur(string isim)
{
    $(HILITE import std.string;)

    string söz = format("Merhaba %s", isim);
    return söz;
}

void kullanıcıylaEtkileş()
{
    $(HILITE import std.stdio;)

    write("Lütfen isminizi girin: ");
    string isim = readln();
    writeln(mesajOluştur(isim));
}

void main()
{
    kullanıcıylaEtkileş();
}
---

$(P
$(C import) satırlarının yerel kapsamlarda bulunmaları modül kapsamında bulunmalarından daha iyidir çünkü derleyici kullanılmayan kapsamlardaki $(C import) satırlarını derlemek zorunda kalmaz. Ek olarak, yerel olarak eklenmiş olan modüllerdeki isimler ancak eklendikleri kapsamda görünürler ve böylece isim çakışmalarının olasılığı da azalmış olur.
)

$(P
Daha sonraki $(LINK2 /ders/d/katmalar.html, Katmalar bölümünde) göreceğimiz $(I şablon katmaları) olanağında modüllerin yerel olarak eklenmeleri şarttır.
)

$(P
Bu kitaptaki örnekler yerel $(C import) olanağından hemen hemen hiç yararlanmazlar çünkü bu olanak D'ye bu kitabın yazılmaya başlanmasından sonra eklenmiştir.
)

$(H5 Modüllerin dosya sistemindeki yerleri)

$(P
Modül isimleri dosya sistemindeki dosyalara bire bir karşılık geldiği için, derleyici bir modül dosyasının nerede bulunduğunu modül ismini klasör ve dosya isimlerine dönüştürerek bulur.
)

$(P
Örneğin yukarıdaki programın kullandığı iki modül, "hayvan/kedi.d" ve "hayvan/kopek.d" dosyalarıdır. Dolayısıyla, yukarıdaki dosyayı da sayarsak bu programı oluşturmak için üç modül kullanılmaktadır.
)

$(H5 Kısa ve uzun isimler)

$(P
Programda kullanılan isimler, paket ve modül bilgilerini de içeren $(I uzun halde) de yazılabilirler. Bunu $(C Kedi) sınıfının tür ismini kısa ve uzun yazarak şöyle gösterebiliriz:
)

---
    auto kedi0 = new Kedi();
    auto kedi1 = new hayvan.kedi.Kedi();  // üsttekinin aynısı
---

$(P
Normalde uzun isimleri kullanmak gerekmez. Onları yalnızca olası karışıklıkları gidermek için kullanırız. Örneğin iki modülde birden tanımlanmış olan bir ismi kısa olarak yazdığımızda, derleyici hangi modüldekinden bahsettiğimizi anlayamaz.
)

$(P
Hem $(C hayvan) modülünde hem de $(C arabalar) modülünde bulunabilecek $(C Jaguar) isimli iki sınıftan hangisinden bahsettiğimizi uzun ismiyle şöyle belirtmek zorunda kalırız:
)

---
import hayvan.jaguar;
import arabalar.jaguar;

// ...

    auto karışıklık =  Jaguar();            $(DERLEME_HATASI)

    auto hayvanım = hayvan.jaguar.Jaguar(); $(CODE_NOTE derlenir)
    auto arabam = arabalar.jaguar.Jaguar(); $(CODE_NOTE derlenir)
---

$(H6 Takma isimle eklemek)

$(P
Modüller kolaylık veya isim çakışmalarını önleme gibi amaçlarla takma isim vererek eklenebilirler:
)

---
import $(HILITE etobur =) hayvan.jaguar;
import $(HILITE araç =) arabalar.jaguar;

// ...

    auto hayvanım = $(HILITE etobur.)Jaguar();       $(CODE_NOTE derlenir)
    auto arabam   = $(HILITE araç.)Jaguar();         $(CODE_NOTE derlenir)
---

$(P
Bütün modüle takma isim vermek yerine seçilen her isme ayrı ayrı takma isim verilebilir.
)

$(P
Bir örnek olarak, aşağıdaki kod $(C -w) derleyici seçeneği ile derlendiğinde derleyici $(C .sort) $(I niteliğinin) değil, $(C sort()) $(I işlevinin) yeğlenmesi yönünde bir uyarı verir:
)

---
import std.stdio;
import std.algorithm;

// ...

    auto dizi = [ 2, 10, 1, 5 ];
    dizi.sort;    $(CODE_NOTE_WRONG derleme UYARISI)
    writeln(dizi);
---

$(SHELL
Warning: use std.algorithm.sort instead of .sort property
)

$(P
$(I Not: Yukarıdaki $(C dizi.sort) ifadesi $(C sort(dizi)) çağrısının eşdeğiridir. Farkı, $(LINK2 /ders/d/ufcs.html, ilerideki bir bölümde) göreceğimiz UFCS söz dizimi ile yazılmış olmasıdır.)
)

$(P
Bu durumda bir çözüm, $(C std.algorithm.sort) işlevinin takma isimle eklenmesidir. Aşağıdaki yeni $(C algSort) ismi $(C sort()) $(I işlevi) anlamına geldiğinden derleyici uyarısına gerek kalmamış olur:
)

---
import std.stdio;
import std.algorithm : $(HILITE algSort =) sort;

void main()
{
    auto arr = [ 2, 10, 1, 5 ];
    arr$(HILITE .algSort);
    writeln(arr);
}
---

$(H5 Pakedi modül olarak eklemek)

$(P
Bazen bir pakedin bir modülü eklendiğinde hep başka modüllerinin de eklenmeleri gerekiyor olabilir. Örneğin, $(C hayvan.kedi) modülü eklendiğinde $(C hayvan.kopek), $(C hayvan.at), vs. modülleri de ekleniyordur.
)

$(P
Böyle durumlarda modülleri tek tek eklemek yerine bütün pakedi veya bir bölümünü eklemek mümkündür:
)

---
import hayvan;    $(CODE_NOTE bütün paket modül gibi ekleniyor)
---

$(P
Bu, ismi $(C package.d) olan özel bir ayar dosyası ile sağlanır. Bu dosyada önce bir $(C module) satırıyla pakedin ismi bildirilir, sonra hep birlikte eklenmeleri gereken modüller $(C public) olarak eklenirler:
)

---
// hayvan/package.d dosyasının içeriği:
module hayvan;

$(HILITE public) import hayvan.kedi;
$(HILITE public) import hayvan.kopek;
$(HILITE public) import hayvan.at;
// ... diğer modüller için de benzer satırlar ...
---

$(P
Bir modülün $(C public) olarak eklenmesi, kullanıcıların eklenen modüldeki isimleri görebilmelerini sağlar. Sonuç olarak, kullanıcılar aslında bir paket olan $(C hayvan) modülünü eklediklerinde $(C hayvan.kedi), $(C hayvan.kopek), vs. modüllere otomatik olarak erişmiş olurlar.
)

$(H5 Modüllerdeki tanımların programa dahil edilmesi)

$(P
$(C import) anahtar sözcüğü belirtilen modülün programın parçası haline gelmesi için yeterli değildir. $(C import), yalnızca o modüldeki olanakların bu kaynak kod içinde kullanılabilmelerini sağlar. O kadarı ancak kaynak kodun $(I derlenebilmesi) için gereklidir.
)

$(P
Yukarıdaki programı yalnızca "deneme.d" dosyasını kullanarak oluşturmaya çalışmak yetmez:
)

$(SHELL_SMALL
$ dmd deneme.d -w
$(DARK_GRAY deneme.o: In function `_Dmain':
deneme.d:(.text._Dmain+0x4): undefined reference to
`_D6hayvan4kedi4Kedi7__ClassZ'
deneme.d:(.text._Dmain+0xf): undefined reference to
`_D6hayvan5kopek6Köpek7__ClassZ'
collect2: ld returned 1 exit status
--- errorlevel 1)
)

$(P
O hata mesajları $(I bağlayıcıdan) gelir. Her ne kadar anlaşılmaz isimler içeriyor olsalar da, yukarıdaki hata mesajları programda kullanılan bazı tanımların bulunamadıklarını bildirir.
)

$(P
Programın oluşturulması, perde arkasında çağrılan bağlayıcının görevidir. Derleyici tarafından derlenen modüller bağlayıcıya verilir ve program bağlayıcının bir araya getirdiği parçalardan oluşturulur.
)

$(P
O yüzden, programı oluşturan bütün parçaların derleme satırında belirtilmeleri gerekir. Yukarıdaki programın oluşturulabilmesi için, kullandığı "hayvan/kedi.d" ve "hayvan/kopek.d" dosyaları da derleme satırında bildirilmelidir:
)

$(SHELL_SMALL
$ dmd deneme.d hayvan/kedi.d hayvan/kopek.d -w
)

$(P
Modülleri derleme satırında her program için ayrı ayrı belirtmek yerine kütüphaneler içinden de kullanabiliriz.
)

$(H5 Kütüphaneler)

$(P
Modül tanımlarının derlendikten sonra bir araya getirilmelerine kütüphane adı verilir. Kütüphaneler kendileri program olmadıklarından, programların başlangıç işlevi olan $(C main) kütüphanelerde bulunmaz. Kütüphaneler yalnızca işlev, yapı, sınıf, vs. $(I tanımlarını) bir araya getirirler. Daha sonra program oluşturulurken programın diğer modülleriyle bağlanırlar.
)

$(P
Kütüphane oluşturmak için dmd'nin $(C -lib) seçeneği kullanılır. Oluşturulan kütüphanenin isminin $(C hayvan) olmasını sağlamak için $(C -of) seçeneğini de kullanırsak, yukarıdaki "kedi.d" ve "kopek.d" modüllerini içeren bir kütüphane şu şekilde oluşturulabilir:
)

$(SHELL_SMALL
$ dmd hayvan/kedi.d hayvan/kopek.d -lib -ofhayvan -w
)

$(P
Konsoldan çalıştırılan o komut, belirtilen .d dosyalarını derler ve bir kütüphane dosyası olarak bir araya getirir. Çalıştığınız ortama bağlı olarak kütüphane dosyasının ismi farklı olacaktır. Örneğin Linux ortamlarında kütüphane dosyalarının uzantıları .a olur: $(C hayvan.a)
)

$(P
Program oluşturulurken artık "hayvan/kedi.d" ve "hayvan/kopek.d"nin ayrı ayrı bildirilmelerine gerek kalmaz. Onları içeren kütüphane dosyası tek başına yeterlidir:
)

$(SHELL_SMALL
$ dmd deneme.d hayvan.a -w
)

$(P
O komut, daha önce kullandığımız şu komutun eşdeğeridir:
)

$(SHELL_SMALL
$ dmd deneme.d hayvan/kedi.d hayvan/kopek.d -w
)

$(P
Bir istisna olarak, şimdiye kadar çok yararlandığımız Phobos modüllerini içeren standart kütüphanenin açıkça bildirilmesi gerekmez. O kütüphane, programa otomatik olarak dahil edilir. Yoksa normalde onu da örneğin şu şekilde belirtmemiz gerekirdi:
)

$(SHELL_SMALL
$ dmd deneme.d hayvan.a /usr/lib64/libphobos2.a -w
)

$(P
$(I Not: Phobos kütüphane dosyasının yeri ve ismi sizin ortamınızda farklı olabilir.)
)

Macros:
        SUBTITLE=Modüller ve Kütüphaneler

        DESCRIPTION=D programlarını oluşturan alt parçalar olan modüllerin, ve onların bir araya gelmelerinden oluşan kütüphanelerin tanıtılması

        KEYWORDS=d programlama dili ders dersler öğrenmek tutorial modül module kütüphane library

SOZLER=
$(baglayici)
$(derleyici)
$(is_parcacigi)
$(kaynak_dosya)
$(klasor)
$(kutuphane)
$(modul)
$(paket)
$(phobos)
$(tanim)
